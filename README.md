# Text Analytics

In this project, I demonstrate the end-to-end machine learning process for some natural language processing tasks, namely text classification and entity extraction. The datasets used to train the machine learning models are from languages commonly spoken in South-East Asia (English, Chinese, Malay, Indonesian).

The following are demonstrated in this repo:

1. Train a DistilBERT model to perform sentiment analysis
   (labels: positive, neutral, negative).
2. Train a multilingual DistilBERT model to perform named entity recognition
   (entities: person, location, organisation).
3. Serve model predictions using FastAPI
4. Create a Streamlit frontend to visualise the model predictions from user input.

TODO:
- Convert all models to ONNX to optimise for inference runtime.
- Train the model to be multilingual, it currently works on English only.

To start the app, run `docker-compose up` at the root of the project. The documentation of API endpoints are  available at `localhost:8555/docs` (sentiment analysis) and `localhost:8557/docs` (named entity recognition), the Streamlit app is available at `localhost:8556`.

Project structure

```
├───data
│   ├───named_entity_recognition
│   │   ├───test
│   │   ├───train
│   │   └───validation
│   └───sentiment_analysis
│       ├───test
│       ├───train
│       └───validation
├───models
│   ├───distilbert-base-uncased
│   ├───distilbert_ner
│   └───distilbert_sentiment
├───notebooks
├───src
│   ├───backend
│   │   ├───named_entity_recognition
│   │   └───sentiment_analysis
│   └───frontend
├───docker-compose.yml
```

- data used for training is available at `./data`
- training scripts and exploration notebooks are available at `./notebooks`
- the trained models are available at `./models`
- the Dockerfiles and scripts used to create the API endpoints are at `./src/backend`
- the Dockerfiles and scripts used to create the Streamlit app are at `./src/frontend`
