.PHONY: start
start: # start containers via docker-compse up
	docker-compose up

.PHONY: testing
testing:  # simple command to test if gitlab CI/CD works
	echo "Hello, it works! :)" && \
	echo "It really works! :D"

.PHONY: yapf-lint
yapf-lint: # Run YAPF diff, displaying recommended formatting changes and raising an error if there are any.
	docker run --rm 			\
		-v $(PWD)/src:/src:Z	\
		--workdir=/src			\
		zechyw/yapf				\
	yapf --style '{based_on_style: pep8, dedent_closing_brackets: true, coalesce_brackets: true}' \
		--no-local-style --verbose --recursive --diff --parallel .
	

.PHONY: yapf-format
yapf-format: # Run YAPF in-place, correct formatting errors in python scripts
	docker run --rm 			\
		-v $(PWD)/src:/src:Z	\
		--workdir=/src			\
		zechyw/yapf  \
	yapf --style '{based_on_style: pep8, dedent_closing_brackets: true, coalesce_brackets: true}' \
		--no-local-style --verbose --recursive --in-place --parallel .