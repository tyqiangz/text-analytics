import streamlit as st
import requests
from annotated_text import annotated_text

st.title('Text Analytics')
st.write(
    'This app allows you to classify a text into 3 sentiment classes (positive, neutral, negative) and extract entities (location, person, organisation). Give it a try below!'
)
SAMPLE_TEXT = "The Eiffel Tower is in Paris, a city in France, whereas Captain Michael's xylophone is in the White House. Wow, the named entity recognition technology by Google is quite good!"
text = st.text_input('Key in some text', SAMPLE_TEXT)

response = requests.post(
    "http://backend_sentiment:8555/get_sentiment", json={"text": text}
)
sentiment = response.json()

st.markdown('#')
st.subheader("Sentiment analysis")

neg_bar_descr = st.empty()
neg_bar = st.progress(0)

pos_bar_descr = st.empty()
pos_bar_descr.text("positive")
pos_bar = st.progress(0)

for sentiment_item in sentiment:
    if sentiment_item["label"] == "negative":
        neg_bar_descr.text(f"negative: {sentiment_item['score'] * 100:.2f}%")
        neg_bar.progress(int(sentiment_item["score"] * 100))

    elif sentiment_item["label"] == "positive":
        pos_bar_descr.text(f"positive: {sentiment_item['score'] * 100:.2f}%")
        pos_bar.progress(int(sentiment_item["score"] * 100))

st.markdown('#')

response = requests.post(
    "http://backend_ner:8557/get_ner_tags", json={"text": text}
)
ner_tags = response.json()

st.subheader("Named Entity Recognition")

# the colors for each tag, the tag should match the tags in `ner_tags[i]["entity_group"]` for each tag `i`
TAG_COLORS = {"LOC": "#fea", "PER": "#8ef", "ORG": "#faa", "MISC": "#afa"}

# reference: https://github.com/tvst/st-annotated-text
# the `annotated_text` functions takes in arguments where each argument is either a text or
# a tuple containing the text, the NER tag and the color for the tag
# we use a list to store all the information to be fed to the `annotated_text` function
formatted_text = []

if len(ner_tags) == 0:
    formatted_text = [text]
else:
    # for each `ner_tag`, we append the text that comes before it, then append the tag
    for i in range(len(ner_tags)):
        if i == 0:
            start_index = 0
        else:
            start_index = ner_tags[i - 1]["end"]

        # append the text with no tag
        end_index = ner_tags[i]["start"]
        formatted_text.append(text[start_index:end_index])

        # append the text with tag
        start_index = ner_tags[i]["start"]
        end_index = ner_tags[i]["end"]
        formatted_text.append((
            text[start_index:end_index], ner_tags[i]["entity_group"],
            TAG_COLORS[ner_tags[i]["entity_group"]]
        ))
    # add the text after the last tag, if any
    start_index = ner_tags[-1]["end"]
    formatted_text.append(text[start_index:])

# display the tags on Streamlit frontend
annotated_text(*formatted_text)

st.markdown('#')
st.markdown('###### Legend:')
st.write(
    "`LOC`: Location, `PER`: Person, `ORG`: Organisation, `MISC`: Miscellaneous"
)
