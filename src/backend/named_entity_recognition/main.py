from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn
from transformers import pipeline

import logging


class TextData(BaseModel):
    text: str


app = FastAPI(title="Testing API", description="Some sample APIs")

classifier = None


@app.on_event("startup")
def startup_event():
    """
    Load the models upon start-up to be used by the APIs
    """
    global classifier
    classifier = pipeline(
        "token-classification",
        model='/models/distilbert_ner',
        framework="pt",
        aggregation_strategy="first"
    )


@app.get("/")
def health_check():
    return "It's working!"


@app.post("/get_ner_tags")
def get_ner_tags(text: TextData):

    tokens = classifier(text.text)

    for i in range(len(tokens)):
        tokens[i]['score'] = float(tokens[i]['score'])

    return tokens


if __name__ == "__main__":
    uvicorn.run("main:app", host='0.0.0.0', port=8557, reload=True)
