from fastapi import FastAPI
from pydantic import BaseModel
import uvicorn
from transformers import pipeline


class TextData(BaseModel):
    text: str


app = FastAPI(title="Testing API", description="Some sample APIs")

classifier = None


@app.on_event("startup")
def startup_event():
    """
    Load the models upon start-up to be used by the APIs
    """
    global classifier
    classifier = pipeline(
        "sentiment-analysis",
        model='/models/distilbert_sentiment',
        return_all_scores=True,
        framework="pt"
    )


@app.get("/")
def health_check():
    return "It's working!"


@app.post("/get_sentiment")
def get_sentiment(text: TextData):
    sentiment = classifier(text.text)[0]

    return sentiment


if __name__ == "__main__":
    uvicorn.run("main:app", host='0.0.0.0', port=8555, reload=False)
