from types import SimpleNamespace
from pathlib import Path
import numpy as np

from typing import Union, Optional
from dataclasses import dataclass
from datasets import load_dataset, load_metric
import torch
from torch import nn
from torch.optim import AdamW

from transformers import AutoConfig, AutoTokenizer, AutoModel
from transformers.tokenization_utils_base import PreTrainedTokenizerBase
from transformers.utils import PaddingStrategy
from transformers.data.data_collator import DataCollatorMixin
from transformers.modeling_outputs import SequenceClassifierOutput
from transformers import TrainingArguments, Trainer, EarlyStoppingCallback, DataCollatorWithPadding

config = SimpleNamespace()

config.seed = 420
config.model_name = 'distilbert-base-multilingual-cased'
config.tmp_path = Path('../models/')
config.output_path = "../models/distilbert_multilingual_sentiment"
config.epochs = 5

config.max_len = 512

config.eval_per_epoch = 2
config.add_prefix_space = False

config.batch_size = 1
config.gradient_accumulation_steps = 4
config.group_by_length = True
config.gradient_checkpointing = False

config.lr = 1e-5
config.backbone_lr_fraction = 1

config.warm_up_ratio = 0.
config.weight_decay = 0.01

config.bilstm_layer = False

config.hidden_dropout_prob = 0.
config.token_dropout_prob = 0.
config.attention_probs_dropout_prob = 0

config.num_labels = 3

# report to weights and biases or not
config.report_to_wandb = True

all_special_tokens = []
tokenizer = AutoTokenizer.from_pretrained(config.model_name, use_fast=True, add_prefix_space=config.add_prefix_space, additional_special_tokens=all_special_tokens, return_special_tokens_mask=True)
tokenizer.model_max_length = config.max_len

dataset = load_dataset("tyqiangz/multilingual-sentiments", "english")


def seed_everything(seed: int):
    """From this Gist: https://gist.github.com/ihoromi4/b681a9088f348942b01711f251e5f964"""
    import random, os
    import numpy as np
    import torch
    
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True

def tokenizer_func(x):
    """
    A function to be applied on HuggingFace's Datasets

    Tokenizes the text input and adds a length variable 
    for the number of tokens generated from the tokenizer.
    """
    output = tokenizer(x["text"], truncation=True)
    output['length'] = [len(a) for a in output['input_ids']]
    return output

@dataclass
class DataCollatorWithTokenDropout(DataCollatorMixin):
    tokenizer: PreTrainedTokenizerBase
    padding: Union[bool, str, PaddingStrategy] = True
    max_length: Optional[int] = None
    pad_to_multiple_of: Optional[int] = None
    return_tensors: str = "pt"
    # percentage of tokens to mask or change to random tokens
    dropout_prob: float = 0.0

    def torch_call(self, features):
        batch = self.tokenizer.pad(
            features,
            padding=self.padding,
            max_length=self.max_length,
            pad_to_multiple_of=self.pad_to_multiple_of,
            return_tensors=self.return_tensors,
        )

        # If special token mask has been preprocessed, pop it from the dict.
        special_tokens_mask = batch.pop("special_tokens_mask", None)
        if self.dropout_prob > 0:
            batch["input_ids"] = self.torch_mask_tokens(
                batch["input_ids"], special_tokens_mask=special_tokens_mask
            )

        if "label" in batch:
            batch["labels"] = batch["label"]
            del batch["label"]
        if "label_ids" in batch:
            batch["labels"] = batch["label_ids"]
            del batch["label_ids"]

        return batch

    def torch_mask_tokens(self, inputs, special_tokens_mask = None):
        import torch

        probability_matrix = torch.full(inputs.shape, self.dropout_prob)
        if special_tokens_mask is None:
            special_tokens_mask = [
                self.tokenizer.get_special_tokens_mask(val, already_has_special_tokens=True) for val in inputs.clone().tolist()
            ]
            special_tokens_mask = torch.tensor(special_tokens_mask, dtype=torch.bool)
        else:
            special_tokens_mask = special_tokens_mask.bool()

        probability_matrix.masked_fill_(special_tokens_mask, value=0.0)
        masked_indices = torch.bernoulli(probability_matrix).bool()\

        # 80% of the time, we replace masked input tokens with tokenizer.mask_token ([MASK])
        indices_replaced = torch.bernoulli(torch.full(inputs.shape, 0.8)).bool() & masked_indices
        inputs[indices_replaced] = self.tokenizer.convert_tokens_to_ids(self.tokenizer.mask_token)

        # 10% of the time, we replace masked input tokens with random word
        indices_random = torch.bernoulli(torch.full(inputs.shape, 0.5)).bool() & masked_indices & ~indices_replaced
        random_words = torch.randint(len(self.tokenizer), inputs.shape, dtype=torch.long)
        inputs[indices_random] = random_words[indices_random]

        # The rest of the time (10% of the time) we keep the masked input tokens unchanged
        return inputs

class MeanPooling(nn.Module):
    def __init__(self):
        super(MeanPooling, self).__init__()
        
    def forward(self, last_hidden_state, attention_mask):
        input_mask_expanded = attention_mask.unsqueeze(-1).expand(last_hidden_state.size()).float()
        sum_embeddings = torch.sum(last_hidden_state * input_mask_expanded, 1)
        sum_mask = input_mask_expanded.sum(1)
        sum_mask = torch.clamp(sum_mask, min=1e-9)
        mean_embeddings = sum_embeddings / sum_mask
        return mean_embeddings

class CustomModel(nn.Module):
    def __init__(self, backbone):
        super(CustomModel, self).__init__()
        
        self.backbone = backbone

        self.config = self.backbone.config

        num_labels = config.num_labels
        self.num_labels = num_labels

        self.mean_pooling = MeanPooling()
        # self.max_pooler = MaxPooling()
        # self.min_pooler = MinPooling()
        
        self.loss_func = nn.CrossEntropyLoss()
        self.softmax = nn.Softmax(dim=1)
        
        if config.bilstm_layer:
            print('Including LSTM layer')
            self.lstm = nn.LSTM(
                self.config.hidden_size,
                (self.config.hidden_size) // 2,
                num_layers=2,
                dropout=self.config.hidden_dropout_prob,
                batch_first=True,
                bidirectional=True)

        self.dropout = nn.Dropout(self.config.hidden_dropout_prob)
        self.classifier = nn.Linear(self.config.hidden_size, num_labels)
    
    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        position_ids=None,
        inputs_embeds=None,
        labels=None,
        output_attentions=None,
        output_hidden_states=None,
        return_dict=None,
        **kwargs
    ):
        
        outputs = self.backbone(
            input_ids,
            # token_type_ids=token_type_ids,
            attention_mask=attention_mask,
            # position_ids=position_ids,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
            return_dict=return_dict,
        )
        
        sequence_output = outputs[0]

        sequence_output = self.dropout(sequence_output)

        if config.bilstm_layer:
            output, _ = self.lstm(sequence_output)
        else:
            output = sequence_output

        mean_pool = self.mean_pooling(output, attention_mask)
        # max_pool = self.max_pooler(output, attention_mask)
        # min_pool = self.min_pooler(output, attention_mask)

        # concat = torch.cat([mean_pool], dim=1)
    
        logits = self.classifier(mean_pool)
        
        loss = None
        if labels is not None:
            logits = logits.view(-1, self.num_labels)
            loss = self.loss_func(logits, labels)

        logits = self.softmax(logits)

        return SequenceClassifierOutput(
                    loss=loss, logits=logits, 
                    hidden_states=outputs.hidden_states, 
                    attentions=outputs.attentions
                )

def get_optimizer_params(model, base_lr, weight_decay=0.0):
    no_decay = ["bias", "LayerNorm.bias", "LayerNorm.weight"]

    print(f'Base lr: {base_lr}')
    backbone_lr = base_lr * config.backbone_lr_fraction
    print(f'Backbone lr: {backbone_lr}')
    optimizer_parameters = [
        {'params': [p for n, p in model.backbone.named_parameters() if not any(nd in n for nd in no_decay)],
            'lr': backbone_lr, 'weight_decay': weight_decay},
        {'params': [p for n, p in model.backbone.named_parameters() if any(nd in n for nd in no_decay)],
            'lr': backbone_lr, 'weight_decay': 0.0},
        {'params': [p for n, p in model.named_parameters() if "backbone" not in n],
            'lr': base_lr, 'weight_decay': 0.0}
    ]
    return optimizer_parameters

class TrainerWithCustomOptim(Trainer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def create_optimizer(self):
        print('Creating custom optimiser.')
        kwargs = {
            "lr": self.args.learning_rate,
            "betas": (self.args.adam_beta1, self.args.adam_beta2),
            "eps": self.args.adam_epsilon,
        }
        self.optimizer = AdamW(get_optimizer_params(self.model, self.args.learning_rate, self.args.weight_decay), **kwargs)
        return self.optimizer

    def get_test_dataloader(self, test_dataset):
        print('Change collate for test')
        test_dataloader = super().get_test_dataloader(test_dataset)
        test_dataloader.collate_fn = DataCollatorWithPadding(tokenizer=self.data_collator.tokenizer, padding='longest')
        return test_dataloader
         
    def get_eval_dataloader(self, eval_dataset=None):
        print('Change collate for eval')
        eval_dataloader = super().get_eval_dataloader(eval_dataset)
        eval_dataloader.collate_fn = DataCollatorWithPadding(tokenizer=self.data_collator.tokenizer, padding='longest')
        return eval_dataloader

def get_config():
    model_config = AutoConfig.from_pretrained(config.model_name, num_labels=config.num_labels)
    model_config.hidden_dropout_prob = config.hidden_dropout_prob
    model_config.attention_probs_dropout_prob = config.attention_probs_dropout_prob
    return model_config

def get_model():
    state_dict = AutoModel.from_pretrained(config.model_name).state_dict()

    model_config = get_config()
    backbone = AutoModel.from_config(model_config)
    backbone.load_state_dict(state_dict)

    if config.gradient_checkpointing:
        print('Setup gradient checkpointing')
        backbone.gradient_checkpointing_enable()
    
    return CustomModel(backbone)   

def compute_metrics(eval_pred):
    metric = load_metric("f1", average="macro")
    predictions, labels = eval_pred
    predictions = predictions.argmax(axis=-1)

    return metric.compute(predictions=predictions, references=labels)

def train():
    train_dataset = load_dataset("tyqiangz/multilingual-sentiments", "english", split="train[:100]")
    val_dataset = load_dataset("tyqiangz/multilingual-sentiments", "english", split="validation[:100]")

    train_tok_dataset = train_dataset.map(tokenizer_func, batched=True, remove_columns=('text'))
    
    val_tok_dataset = val_dataset.map(tokenizer_func, batched=True, remove_columns=('text'))

    # sort data by length for faster inference speed by minimising padding
    val_tok_dataset = val_tok_dataset.sort('length', reverse=True).remove_columns('length')

    data_collator = DataCollatorWithTokenDropout(tokenizer=tokenizer, padding='longest', dropout_prob=config.token_dropout_prob)

    args = TrainingArguments(
        output_dir=config.output_path,
        learning_rate=config.lr,
        warmup_ratio=config.warm_up_ratio,
        lr_scheduler_type='cosine',
#         fp16=True,
        per_device_train_batch_size=config.batch_size,
        per_device_eval_batch_size=config.batch_size,
        num_train_epochs=config.epochs,
        weight_decay=config.weight_decay,
        evaluation_strategy='epoch',
        save_strategy='epoch',
        logging_strategy="epoch",
        load_best_model_at_end=True,
        gradient_accumulation_steps=config.gradient_accumulation_steps,
        optim="adamw_torch",
        group_by_length=config.group_by_length,
        seed=config.seed,
        save_total_limit=3,
        report_to="wandb" if config.report_to_wandb else "none",
    )

    model = get_model()

    early_stopping = EarlyStoppingCallback(early_stopping_patience=5)
    
    trainer = TrainerWithCustomOptim(
        model,
        args,
        train_dataset=train_tok_dataset,
        eval_dataset=val_tok_dataset,
        tokenizer=tokenizer,
        data_collator=data_collator,
        callbacks=[early_stopping],
        compute_metrics=compute_metrics
    )

    trainer.train()
    
    trainer.save_model(config.tmp_path)
    
    outputs = trainer.predict(val_tok_dataset)

    predictions = np.argmax(outputs.predictions, axis=1)

    # val_preds_df = pd.concat([
    #     pd.DataFrame({'text_id': val_tok_dataset['text_id']}),
    #     pd.DataFrame(val_preds, columns=LABEL_COLUMNS)], axis=1)
    # val_data = val_data.drop(columns=LABEL_COLUMNS).merge(val_preds_df, on='text_id', how='left').reset_index(drop=True)

    # return val_data

if __name__ == "__main__":
    train()